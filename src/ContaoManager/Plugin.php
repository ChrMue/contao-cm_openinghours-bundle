<?php

declare(strict_types=1);

/*
 * PHP version 7
 * @copyright  Christian Muenster 2022 
 * @author     Christian Muenster 
 * @package    CM_Openinghours
 * @license    LGPL 
 * @filesource
 */

namespace ChrMue\CmOpeninghoursBundle\ContaoManager;


use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;

/**
 * @internal
 */
class Plugin implements BundlePluginInterface
{
    public function getBundles(ParserInterface $parser): array
    {
        return [
            BundleConfig::create('ChrMue\CmOpeninghoursBundle\ChrMueCmOpeninghoursBundle')
                ->setLoadAfter(['Contao\CoreBundle\ContaoCoreBundle'])
/*                ->setReplace(['cm_openinghours']),*/
        ];
    }
}
