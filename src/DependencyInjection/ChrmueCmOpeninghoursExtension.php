<?php

declare(strict_types=1);

/*
 * PHP version 7
 * @copyright  Christian Muenster 2022
 * @author     Christian Muenster
 * @package    CM_Openinghours
 * @license    LGPL
 * @filesource
 */

namespace ChrMue\CmOpeninghoursBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class ChrmueCmOpeninghoursExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );

        $loader->load('listener.yml');
        $loader->load('services.yml');
    }
}
