<?php

declare(strict_types=1);

/**
 * PHP version 7
 * @copyright  Christian Muenster 2022 
 * @author     Christian Muenster 
 * @package    CM_Openinghours
 * @license    LGPL 
 * @filesource
 */ 
namespace ChrMue\CmOpeninghoursBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Configures the ChrMue CmOpeninghours bundle.
 *
 * @author Christian Muenster
 */
class ChrMueCmOpeninghoursBundle extends Bundle
{}
