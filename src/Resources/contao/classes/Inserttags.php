<?php

/**
 * PHP version 7
 * @copyright  Christian Muenster 2022
 * @author     Christian Muenster
 * @package    CM_Openinghours
 * @license    LGPL
 * @filesource
 */

namespace ChrMue\cm_OpeningHours;

class Inserttags extends \Contao\Frontend
{

    public function cm_ReplaceInsertTags($strTag)
    {
        // separate tag name from options
        $arrSplit = explode('::', $strTag);
        $name = $arrSplit[0];
        
        switch ($name) {
            case 'cm_openingstatus': return $this->replaceOpeningStatus($arrSplit); 
                break;
            case 'cm_openinghourslist': return $this->replaceOpeningHoursList($arrSplit); 
                break;
            default: return false;
                break;
        }
        return false;
    }
    
    private function replaceOpeningStatus($parameters)
    {
        global $objPage;
        if (isset($parameters[1]) && $parameters[1] != '')
        {
            $id = $parameters[1];
            $arrDetailSplit = explode(',', $id);
            $reqid = $arrDetailSplit[0];
            
            //options: 
            // 0 do not show next
            // 1 show next opening if closed
            // 2 closing tinme if opend
            // 3 show next opening if closed and closing tinme if opend
            $showNext=0;
            if (isset($arrDetailSplit[1]))
            {
                $showNext=  intval($arrDetailSplit[1]);
                if ($showNext > 3) {$showNext = 0;}
            }
            
//             if (!$reqid && !isset($_GET[$this->detailParam]) && \Contao\Config::get('useAutoItem') && isset($_GET['auto_item']))
//             {
//                 $reqid = \Input::get('auto_item');
//             }
            $currentDayOfTheWeek= date('w');
            $today = OpeningHelper::today();
            $time = OpeningHelper::now($today);
            
            $currentlyOpened = OpeningHelper::getCurrentStatus($reqid, $currentDayOfTheWeek, $today);
            $status = ($currentlyOpened ? 
                    '<span class="open-today">'.$GLOBALS['TL_LANG']['MSC']['cm_openinghours_open'] : 
                    '<span class="closed-today">'.$GLOBALS['TL_LANG']['MSC']['cm_openinghours_closed']);

            if (!$currentlyOpened)
            {
                if (($showNext & 2)==2)
                {
                    $nextOpening = OpeningHelper::getNextOpening($reqid, $currentDayOfTheWeek, $today);
                    
                    if ($nextOpening)
                    {
                        $opensFromStr = \Contao\Date::parse($objPage->timeFormat, $nextOpening->openfrom);
                        $nextDay = '';
                        if ($nextOpening->dayofweek!=$currentDayOfTheWeek || $nextOpening->openfrom < $time)
                        {
                            $nextDay = \sprintf($GLOBALS['TL_LANG']['MSC']['cm_openingstatus_closed_day'],$GLOBALS['TL_LANG']['DAYS'][$nextOpening->dayofweek]); 
                        }
                        $status .= '</span><span class="opens-next">'. \sprintf($GLOBALS['TL_LANG']['MSC']['cm_openingstatus_closed_ext'], $nextDay,$opensFromStr);
                     }
                }
            }
            else 
            {
                if (($showNext & 1)==1)
                {
                    $nextClosing = OpeningHelper::getNextClosing($reqid, $currentDayOfTheWeek, $today);
                    
                    if ($nextClosing)
                    {
                        $closesAtStr = \Contao\Date::parse($objPage->timeFormat, $nextClosing->openuntil);
                        $status .= '</span><span class="closes-next">'. \sprintf($GLOBALS['TL_LANG']['MSC']['cm_openingstatus_open_ext'], $closesAtStr);
                    }
                }
            }
            return $status.'</span>';
            
        }
        return '';
    }
    private function replaceOpeningHoursList($parameters)
    {
//        print_r($parameters);
        global $objPage;
        if (isset($parameters[1]) && $parameters[1] != '')
        {
            $optionList = $parameters[1];
            $options = explode(',', $optionList);
            $mbrId = (int)$options[0];
            if ($mbrId == 0) {
                return;
            }
            $dayLongFormat = (isset($options[1]) && $options[1] == '1');
            $timeFormat = (isset($options[2]) && $options[2] != '') ? $options[2] : $objPage->timeFormat;
            $startOnMonday = (isset($options[3]) && $options[3] == '1');
            $this->ElementTemplate = new \Contao\FrontendTemplate('ce_cm_member_openinghours');
            $this->ElementTemplate->dayNames = $GLOBALS['TL_LANG'][$dayLongFormat ? 'DAYS': 'DAYS_SHORT'];
            $this->ElementTemplate->timeFormat = $timeFormat;
            $this->ElementTemplate->mbrId = $mbrId;
            $retArr=array();
            $arr = OpeningHelper::getOpeningArr($mbrId);
            foreach ($arr as $x=>$items)
            {
                if ($x == 0 && $startOnMonday) {
                    continue;
                }
                $retArr[$x]=$items;
            }
            if ($startOnMonday) 
            {
                $retArr[0]=$arr[0];
            }
            $this->ElementTemplate->openinghours=$retArr;
            return preg_replace('/[\r\n\t]+/', ' ', $this->ElementTemplate->parse());
        }
        return '';
        
    }
    
}