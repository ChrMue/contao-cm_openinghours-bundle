<?php

/**
 * PHP version 7
 * @copyright  Christian Muenster 2022 
 * @author     Christian Muenster 
 * @package    CM_Openinghours
 * @license    LGPL 
 * @filesource
 */

namespace ChrMue\cm_OpeningHours;

class OpeningHelper extends \Contao\Frontend
{
    public static function now(\Contao\Date $today=null)
    {
        if ($today==null) $today=new \Contao\Date();
        $timeObj = new \Contao\Date($today->time,\Contao\Date::getNumericTimeFormat());
        
        return $timeObj->tstamp;
    }
    public static function today()
    {
        return new \Contao\Date();
    }
    
    
    private static function initAllClosed()
    {
        $arr=array();
        for ($i = 0; $i < 7; $i++) {
            $arr[$i]= null;
        }
        return $arr;
    }
    public static function getOpeningArr(int $mbrId)
    {
        $tstamp = static::today()->tstamp;
        $openObj=\Contao\Database::getInstance()
        ->prepare("SELECT `dayofweek`,`openfrom`,`openuntil`FROM`tl_openinghours`WHERE`pid`=?"
            ." AND`published`='1'AND(`start`=''OR`start`<='$tstamp')AND(`stop`=''OR`stop`>'$tstamp')"
            . "ORDER BY`dayofweek`,`openfrom`,`openuntil`")
            ->execute($mbrId);
            $arr=static::initAllClosed();
            
            while ($openObj->next())
            {
                $arr[$openObj->dayofweek][] = array(
                    'openfrom'=> $openObj->openfrom,
                    'openuntil'=> $openObj->openuntil
                );
            }
            return $arr;
    }
    
    public static function getCurrentStatus(int $mbrId, int $currentDayOfTheWeek, $dateTime = null )
    {
        if ($dateTime==null) 
        {
            $dateTime = static::today();
        }
        $time = static::now($dateTime);
        $tstamp = $dateTime->tstamp;
        $opendObj=\Contao\Database::getInstance()
            ->prepare("SELECT count(*) AS \"num\" FROM tl_openinghours WHERE pid=? AND dayofweek=? AND openfrom<? AND openuntil>?"
                ." AND published='1' AND (start='' OR start<='$tstamp') AND (stop='' OR stop>'$tstamp')")
                ->execute($mbrId, $currentDayOfTheWeek, $time, $time);
        $opendObj->next();
        return ($opendObj->num > 0);
    }
    
    public static function getNextOpening(int $mbrId, int $currentDayOfTheWeek, $dateTime = null)
    {
        if ($dateTime==null)
        {
            $dateTime = static::today();
        }
        $time = static::now($dateTime);
        $tstamp = $dateTime->tstamp;
        
        $opensNextObj=\Contao\Database::getInstance()
            ->prepare("SELECT X.* FROM (SELECT dayofweek, (0+dayofweek-?+6) % 7 AS 'sortVal', openfrom FROM tl_openinghours WHERE pid=?"
                ." AND published='1' AND (start='' OR start<='$tstamp') AND (stop='' OR stop>'$tstamp')"
                ." UNION SELECT dayofweek, -1 AS 'sortVal', openfrom FROM tl_openinghours WHERE pid=? AND dayofweek=? AND openfrom>?"
                ." AND published='1' AND (start='' OR start<='$tstamp') AND (stop='' OR stop>'$tstamp')"
                ." ) AS X order by sortVal")
                ->limit(1)
                ->execute($currentDayOfTheWeek, $mbrId, $mbrId, $currentDayOfTheWeek, $time);  

        return $opensNextObj->numRows== 0 ? null : $opensNextObj->next();    
     }
     public static function getNextClosing(int $mbrId, int $currentDayOfTheWeek, $dateTime = null)
     {
         if ($dateTime==null)
         {
             $dateTime = static::today();
         }
         $time = static::now($dateTime);
         $tstamp = $dateTime->tstamp;
         
         $closeNextObj=\Contao\Database::getInstance()
         ->prepare('SELECT * FROM tl_openinghours WHERE pid=? AND dayofweek=? AND openfrom<? AND openuntil>?'
             ." AND published='1' AND (start='' OR start<='$tstamp') AND (stop='' OR stop>'$tstamp')"
             ." ORDER BY openuntil desc")
             ->execute($mbrId, $currentDayOfTheWeek, $time, $time);
         return $closeNextObj->numRows== 0 ? null : $closeNextObj->next();
     }
     
}
class_alias(OpeningHelper::class, 'OpeningHelper');
