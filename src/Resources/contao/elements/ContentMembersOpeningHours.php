<?php

/**
 * PHP version 7
 * @copyright  Christian Muenster 2022 
 * @author     Christian Muenster 
 * @package    CM_Openinghours
 * @license    LGPL 
 * @filesource
 */

namespace ChrMue\cm_OpeningHours;

use Patchwork\Utf8;

class ContentMembersOpeningHours extends \Contao\ContentElement
{
	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'ce_cm_member_openinghours';

	/**
	 * Display a wildcard in the back end
	 *
	 * @return string
	 */
	public function generate()
	{
	    $request = \Contao\System::getContainer()->get('request_stack')->getCurrentRequest();

		if ($request && \Contao\System::getContainer()->get('contao.routing.scope_matcher')->isBackendRequest($request))
		{
			$objTemplate = new \Contao\BackendTemplate('be_wildcard');
			$objTemplate->wildcard = '### ' . Utf8::strtoupper($GLOBALS['TL_LANG']['FMD']['cm_member_openinghours'][0]) . ' ###';
			$objTemplate->title = $this->headline;

			return $objTemplate->parse();
		}

		return parent::generate();
	}

	/**
	 * Generate the module
	 */
	protected function compile()
	{
	    global $objPage;
	    
	    $mbrId = 5;
	    $this->Template->timeFormat = $objPage->timeFormat;
	    
	    $this->Template->mbrId = $mbrId;
	    $this->Template->openinghours = OpeningHelper::getOpeningArr($mbrId);
	}
}

class_alias(ContentMembersOpeningHours::class, 'ContentMembersOpeningHours');
