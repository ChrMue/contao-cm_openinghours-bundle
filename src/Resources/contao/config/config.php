<?php

/**
 * PHP version 7
 * @copyright  Christian Muenster 2022 
 * @author     Christian Muenster 
 * @package    CM_Openinghours
 * @license    LGPL 
 * @filesource
 */


/**
 * Front end modules
 */


$GLOBALS['BE_MOD']['accounts']['member']['tables'][]= 'tl_openinghours';
$GLOBALS['TL_CTE']['includes']['cm_openinghours'] = 'ChrMue\cm_OpeningHours\ContentMembersOpeningHours';

$GLOBALS['TL_MODELS']['tl_openinghours']  = 'ChrMue\cm_OpeningHours\OpeninghoursModel'; 
$GLOBALS['TL_PERMISSIONS'][] = 'member';

$GLOBALS['BE_MOD']['accounts']['member']['defOpeningHours'] = array('Opening', 'defOpeningHours');

$GLOBALS['TL_HOOKS']['replaceInsertTags'][] = array('ChrMue\cm_OpeningHours\Inserttags', 'cm_ReplaceInsertTags');

//$GLOBALS['TL_DCA']['tl_member']['config']['ctable'][] = 'tl_openinghours';