<?php

/*
 * PHP version 7
 * @copyright  Christian Muenster 2022 
 * @author     Christian Muenster 
 * @package    CM_Openinghours
 * @license    LGPL 
 * @filesource
 */



/**
 * Run in a custom namespace, so the class can be replaced
 */
namespace ChrMue\cm_OpeningHours;


class OpeninghoursModel extends \Contao\Model
{
	/**
	 * Table name
	 * @var string
	 */
	protected static $strTable = 'tl_openinghours';

}
class_alias(OpeninghoursModel::class, 'OpeninghoursModel');
