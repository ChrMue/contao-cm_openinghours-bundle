<?php

/**
 * PHP version 7
 * @copyright  Christian Muenster 2022 
 * @author     Christian Muenster 
 * @package    CM_Openinghours
 * @license    LGPL 
 * @filesource
 */

/**
 * Add palettes to tl_openinghours
 */
//use \Contao\CoreBundle\DataContainer\PaletteManipulator;
//use ChrMue\cm_OpeningHours\OpeninghoursModel;

Contao\System::loadLanguageFile('tl_openinghours');
$GLOBALS['TL_DCA']['tl_openinghours'] = array
(
    // Config
    'config' => array
    (
        'dataContainer'               => 'Table',
        'ptable'                      => 'tl_member',
        'switchToEdit'                => true,
        'enableVersioning'            => true,
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary',
                'pid,dayofweek,openfrom,openuntil' => 'index'
            )
        )
    ),
    'list' => array
    (
        'sorting' => array
        (
            'mode'                    => 4,
            'flag'                    => 11,
            'fields'                  => array('dayOfweek'),
            'headerFields'            => array('lastname', 'firstname'),
            'panelLayout'             => 'filter;sort,search,limit',
            'child_record_callback'   => array('tl_openinghours', 'listOpeninghours')
        ),
        'label' => array
        (
            'fields'                  => array('dayOfweek','openfrom','openuntil'),
            'format'                  => '%s: %s - %s',
        ),
        'global_operations' => array
        (
            'all' => array
            (
                'href'              => 'act=select',
                'class'             => 'header_edit_all',
                'attributes'        => 'onclick="Backend.getScrollOffset()" accesskey="e"'
            )
        ),
        'operations' => array
        (
            'edit' => array
            (
                'href'              => 'act=edit',
                'icon'              => 'edit.svg'
            ),
            'copy' => array
            (
                'href'              => 'act=paste&amp;mode=copy',
                'icon'              => 'copy.svg'
            ),
            'cut' => array
            (
                'href'              => 'act=paste&amp;mode=cut',
                'icon'              => 'cut.svg',
                'attributes'        => 'onclick="Backend.getScrollOffset()"'
            ),
            'delete' => array
            (
                'href'              => 'act=delete',
                'icon'              => 'delete.svg',
                'attributes'        => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
            ),
            'toggle' => array
            (
                'icon'              => 'visible.svg',
                'attributes'        => 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s)"',
                'button_callback'   => array('tl_openinghours', 'toggleIcon'),
                'showInHeader'      => true
            ),
            'show' => array
            (
                'href'              => 'act=show',
                'icon'              => 'show.svg'
            )
        )
    ),
    // Palettes
    'palettes' => array
    (
        'default'                   => '{date_legend},dayofweek,openfrom,openuntil;{publish_legend},published,start,stop'
    ),
    'fields' => array
    (
        'id' => array
        (
            'sql'                   => "int(10) unsigned NOT NULL auto_increment"
        ),
        'pid' => array
        (
            'foreignKey'            => 'tl_member.lastname',
            'sql'                   => "int(10) unsigned NOT NULL default 0",
            'relation'              => array('type'=>'belongsTo', 'load'=>'lazy')
        ),
        'sorting' => array
        (
            'sql'                   => "int(10) unsigned NOT NULL default '0'"
        ),
        'tstamp' => array
        (
            'sql'                   => "int(10) unsigned NOT NULL default 0"
        ),       
        'dayofweek' => array(
            'label'                 => &$GLOBALS['TL_LANG']['tl_openinghours']['dayofweek'],
            'inputType'             => 'select',
            'default'               => false,
            'options'               => array(1, 2, 3, 4, 5, 6, 0),
            'reference'             => &$GLOBALS['TL_LANG']['DAYS'],
            'filter'                => true,
            'sorting'               => true,
            'search'                => true,
            'eval'                  => array('mandatory'=>true,'tl_class'=>'w50'),
            'sql'                   => "varchar(10) NOT NULL default 'no'"
        ),
        'openfrom' => array(
            'label'                 => &$GLOBALS['TL_LANG']['tl_openinghours']['openfrom'],
            'default'               => time(),
            'exclude'               => true,
            'filter'                => true,
            'sorting'               => true,
            'flag'                  => 8,
            'inputType'             => 'text',
            'eval'                  => array('rgxp'=>'time', 'mandatory'=>true, 'tl_class'=>'w50 clr'),
            'sql'                   => "int(10) NULL"
        ),
        'openuntil' => array(
            'label'                 => &$GLOBALS['TL_LANG']['tl_openinghours']['openuntil'],
            'default'               => time(),
            'exclude'               => true,
            'filter'                => true,
            'sorting'               => true,
            'flag'                  => 8,
            'inputType'             => 'text',
            'eval'                  => array('rgxp'=>'time', 'mandatory'=>true, 'tl_class'=>'w50'),
            'sql'                   => "int(10) NULL"
        ),
        'published' => array
        (
            'exclude'               => true,
            'filter'                => true,
            'flag'                  => 2,
            'inputType'             => 'checkbox',
            'eval'                  => array('doNotCopy'=>true),
            'sql'                   => "char(1) NOT NULL default ''"
        ),
        'start' => array
        (
            'exclude'               => true,
            'inputType'             => 'text',
            'eval'                  => array('rgxp'=>'datim', 'datepicker'=>true, 'tl_class'=>'w50 wizard'),
            'sql'                   => "varchar(10) NOT NULL default ''"
        ),
        'stop' => array
        (
            'exclude'               => true,
            'inputType'             => 'text',
            'eval'                  => array('rgxp'=>'datim', 'datepicker'=>true, 'tl_class'=>'w50 wizard'),
            'sql'                   => "varchar(10) NOT NULL default ''"
        )
        )
    );

class tl_openinghours extends \Contao\Backend
{
    /**
     * Import the back end user object
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('Contao\BackendUser', 'User');
    } 

    public function listOpeninghours($arrRow)
    { 

        $htmlContent= sprintf('<div><span class="dayoftheweek" >%s</span> '
            .'<span>%s</span> - <span>%s</span></div>',
            $GLOBALS['TL_LANG']['DAYS'][$arrRow['dayofweek']],
            date('H:i', $arrRow['openfrom']),
            date('H:i', $arrRow['openuntil'])
            );
        
        return $htmlContent;
    }
    
    
    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {
        if (Contao\Input::get('tid'))
        {
            $this->toggleVisibility(\Contao\Input::get('tid'), (\Contao\Input::get('state') == 1), (func_num_args() <= 12 ? null : func_get_arg(12)));
            $this->redirect($this->getReferer());
        }
        
        if (!$this->User->hasAccess('tl_openinghours::published', 'alexf'))
        {
            return '';
        }
        
        $href .= '&amp;tid=' . $row['id'] . '&amp;state=' . ($row['published'] ? '' : 1);
        
        if (!$row['published'])
        {
            $icon = 'invisible.svg';
        }
        
        return '<a href="' . $this->addToUrl($href) . '" title="' 
                . \Contao\StringUtil::specialchars($title) . '"' . $attributes . '>' 
                . \Contao\Image::getHtml($icon, $label, 'data-state="' . ($row['published'] ? 1 : 0) . '"') . '</a> ';
    }
    
    /**
     * Disable/enable a user group
     *
     * @param integer              $intId
     * @param boolean              $blnVisible
     * @param Contao\DataContainer $dc
     *
     * @throws Contao\CoreBundle\Exception\AccessDeniedException
     */
    public function toggleVisibility($intId, $blnVisible, Contao\DataContainer $dc=null)
    {
        // Set the ID and action
        Contao\Input::setGet('id', $intId);
        Contao\Input::setGet('act', 'toggle');
        
        if ($dc)
        {
            $dc->id = $intId; // see #8043
        }
        
        // Trigger the onload_callback
        if (is_array($GLOBALS['TL_DCA']['tl_openinghours']['config']['onload_callback']))
        {
            foreach ($GLOBALS['TL_DCA']['tl_openinghours']['config']['onload_callback'] as $callback)
            {
                if (is_array($callback))
                {
                    $this->import($callback[0]);
                    $this->{$callback[0]}->{$callback[1]}($dc);
                }
                elseif (is_callable($callback))
                {
                    $callback($dc);
                }
            }
        }
        
        // Check the field access
        if (!$this->User->hasAccess('tl_openinghours::published', 'alexf'))
        {
            throw new \Contao\CoreBundle\Exception\AccessDeniedException('Not enough permissions to publish/unpublish event ID ' . $intId . '.');
        }
        
        $objRow = $this->Database->prepare("SELECT * FROM tl_openinghours WHERE id=?")
        ->limit(1)
        ->execute($intId);
        
        if ($objRow->numRows < 1)
        {
            throw new \Contao\CoreBundle\Exception\AccessDeniedException('Invalid event ID ' . $intId . '.');
        }
        
        // Set the current record
        if ($dc)
        {
            $dc->activeRecord = $objRow;
        }
                
        $time = time();
        
        // Update the database
        $this->Database->prepare("UPDATE tl_openinghours SET tstamp=$time, published='" . ($blnVisible ? '1' : '') . "' WHERE id=?")
        ->execute($intId);
        
        if ($dc)
        {
            $dc->activeRecord->tstamp = $time;
            $dc->activeRecord->published = ($blnVisible ? '1' : '');
        }
        
        // Trigger the onsubmit_callback
        if (is_array($GLOBALS['TL_DCA']['tl_openinghours']['config']['onsubmit_callback']))
        {
            foreach ($GLOBALS['TL_DCA']['tl_openinghours']['config']['onsubmit_callback'] as $callback)
            {
                if (is_array($callback))
                {
                    $this->import($callback[0]);
                    $this->{$callback[0]}->{$callback[1]}($dc);
                }
                elseif (is_callable($callback))
                {
                    $callback($dc);
                }
            }
        }
                
        if ($dc)
        {
            $dc->invalidateCacheTags();
        }
    }
    
}