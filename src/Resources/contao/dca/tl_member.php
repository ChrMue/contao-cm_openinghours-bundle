<?php

/**
 * PHP version 7
 * @copyright  Christian Muenster 2022 
 * @author     Christian Muenster 
 * @package    CM_Openinghours
 * @license    LGPL 
 * @filesource
 */

$GLOBALS['TL_DCA']['tl_member']['list']['operations']['defOpeningHours'] = array(
    'label' => &$GLOBALS['TL_LANG']['tl_member']['defOpeningHours'],
    'href' => 'table=tl_openinghours',
     'icon' => 'system/themes/flexible/icons/news.svg'
);

