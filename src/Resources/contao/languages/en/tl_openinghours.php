<?php
/** 
 * Contao extension: cm_openinghours
 * Copyright : &copy; 2022 Christian Münster 
 * License   : GNU Lesser Public License (LGPL) 
 * Author    : Christian Münster (ChrMue) 
 * Translator: Christian Münster (ChrMue) 
 * 
 */
$GLOBALS['TL_LANG']['tl_openinghours']['new']  = array('New opening time', 'Create a new opening time');
$GLOBALS['TL_LANG']['tl_openinghours']['copy'] = array('Duplicate opening time', 'Duplicate opening time ID %s');
$GLOBALS['TL_LANG']['tl_openinghours']['cut'] = array('Move opening time', 'Move opening time ID %s');
$GLOBALS['TL_LANG']['tl_openinghours']['show'] = array('Opening time details','Show the details of opening time ID %s anzeigen');
$GLOBALS['TL_LANG']['tl_openinghours']['delete'] = array('Delete opening time', 'Delete opening time ID %s');
$GLOBALS['TL_LANG']['tl_openinghours']['edit'] = array('Edit opening time', 'Edit opening time ID %s');

$GLOBALS['TL_LANG']['tl_openinghours']['date_legend'] = "Opening Hours";
$GLOBALS['TL_LANG']['tl_openinghours']['dayofweek'][0] = "Weekday";
$GLOBALS['TL_LANG']['tl_openinghours']['dayofweek'][1] = "Select the day of the week.";

$GLOBALS['TL_LANG']['tl_openinghours']['openfrom'][0] = "open from";
$GLOBALS['TL_LANG']['tl_openinghours']['openfrom'][1] = "Enter the time to open.";
$GLOBALS['TL_LANG']['tl_openinghours']['openuntil'][0] = "open until";
$GLOBALS['TL_LANG']['tl_openinghours']['openuntil'][1] = "Enter the time to close.";

$GLOBALS['TL_LANG']['tl_openinghours']['publish_legend'] = "Publish settings";
$GLOBALS['TL_LANG']['tl_openinghours']['published'][0] = "Publish opening time";
$GLOBALS['TL_LANG']['tl_openinghours']['published'][1] = "Make the opening time publicly visible on the website.";
$GLOBALS['TL_LANG']['tl_openinghours']['start'][0] = "Show from";
$GLOBALS['TL_LANG']['tl_openinghours']['start'][1] = "If you want to prevent the openming time from showing on the website before a certain date/time, you can enter it here. Otherwise leave the field blank.";
$GLOBALS['TL_LANG']['tl_openinghours']['stop'][0] = "Show until";
$GLOBALS['TL_LANG']['tl_openinghours']['stop'][1] = "If you want to prevent the openming time from showing on the website after a certain date/time, you can enter it here. Otherwise leave the field blank..";

