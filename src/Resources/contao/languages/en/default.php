<?php
/**
 * Contao extension: cm_openinghours
 * 
 * Copyright : &copy; 2022 Christian Münster 
 * License   : GNU Lesser Public License (LGPL) 
 * Author    : Christian Münster (ChrMue) 
 * Translator: Christian Münster (ChrMue) 
 * 
 */
 
$GLOBALS['TL_LANG']['MSC']['cm_openinghours_closed'] = "Closed";
$GLOBALS['TL_LANG']['MSC']['cm_openingstatus_closed_ext'] = ", opens%s at %s.";
$GLOBALS['TL_LANG']['MSC']['cm_openingstatus_closed_day'] = " on %s";
$GLOBALS['TL_LANG']['MSC']['cm_openingstatus_open'] = "Open";
$GLOBALS['TL_LANG']['MSC']['cm_openingstatus_open_ext'] = ", closes at %s";

