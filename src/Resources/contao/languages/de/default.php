<?php
/**
 * Contao extension: cm_openinghours
 * 
 * Copyright : &copy; 2022 Christian Münster 
 * License   : GNU Lesser Public License (LGPL) 
 * Author    : Christian Münster (ChrMue) 
 * Translator: Christian Münster (ChrMue) 
 * 
 */
 
$GLOBALS['TL_LANG']['MSC']['cm_openinghours_closed'] = "Geschlossen";
$GLOBALS['TL_LANG']['MSC']['cm_openingstatus_closed_ext'] = "geschlossen, öffnet%s um %s Uhr.";
$GLOBALS['TL_LANG']['MSC']['cm_openingstatus_closed_day'] = " am %s";
$GLOBALS['TL_LANG']['MSC']['cm_openingstatus_open'] = "geöffnet";
$GLOBALS['TL_LANG']['MSC']['cm_openingstatus_open_ext'] = "geöffnet, schließt um %s Uhr.";

