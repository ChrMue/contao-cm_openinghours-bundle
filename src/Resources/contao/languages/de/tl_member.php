<?php
/** 
 * Contao extension: cm_openinghours
 * Copyright : &copy; 2022 Christian Münster 
 * License   : GNU Lesser Public License (LGPL) 
 * Author    : Christian Münster (ChrMue) 
 * Translator: Christian Münster (ChrMue) 
 * 
 */
 
$GLOBALS['TL_LANG']['tl_member']['defOpeningHours'] = "Öffnungszeiten zu Mitglied ID %s";
