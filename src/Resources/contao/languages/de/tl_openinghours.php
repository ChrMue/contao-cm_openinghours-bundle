<?php
/** 
 * Contao extension: cm_openinghours
 * Copyright : &copy; 2022 Christian Münster 
 * License   : GNU Lesser Public License (LGPL) 
 * Author    : Christian Münster (ChrMue) 
 * Translator: Christian Münster (ChrMue) 
 * 
 */
$GLOBALS['TL_LANG']['tl_openinghours']['new']  = array('Neue Öffnungszeit', 'Eine neue Öffnungszeit erstellen');
$GLOBALS['TL_LANG']['tl_openinghours']['copy'] = array('Öffnungszeit duplizieren', 'Öffnungszeit ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_openinghours']['cut'] = array('Öffnungszeit verschieben', 'Öffnungszeit ID %s verschieben');
$GLOBALS['TL_LANG']['tl_openinghours']['show'] = array('Öffnungszeit-Details','Details der Öffnungszeit ID %s anzeigen');
$GLOBALS['TL_LANG']['tl_openinghours']['delete'] = array('Öffnungszeit löschen', 'Öffnungszeit ID %s löschen');
$GLOBALS['TL_LANG']['tl_openinghours']['edit'] = array('Öffnungszeit bearbeiten', 'Öffnungszeit ID %s bearbeiten');

$GLOBALS['TL_LANG']['tl_openinghours']['date_legend'] = "Öffnungszeiten";
$GLOBALS['TL_LANG']['tl_openinghours']['dayofweek'][0] = "Wochentag";
$GLOBALS['TL_LANG']['tl_openinghours']['dayofweek'][1] = "Wählen Sie den Wochentag aus.";

$GLOBALS['TL_LANG']['tl_openinghours']['openfrom'][0] = "geöffnet von";
$GLOBALS['TL_LANG']['tl_openinghours']['openfrom'][1] = "Geben Sie die Zeit ein, zu der geöffnet werden soll";
$GLOBALS['TL_LANG']['tl_openinghours']['openuntil'][0] = "geöffnet bis";
$GLOBALS['TL_LANG']['tl_openinghours']['openuntil'][1] = "Geben Sie die Zeit ein, zu der geschlossen werden soll.";


$GLOBALS['TL_LANG']['tl_openinghours']['publish_legend'] = "Veröffentlichung";
$GLOBALS['TL_LANG']['tl_openinghours']['published'][0] = "Öffnungszeit veröffentlichen";
$GLOBALS['TL_LANG']['tl_openinghours']['published'][1] = "Die Öffnungszeit auf der Webseite anzeigen.";
$GLOBALS['TL_LANG']['tl_openinghours']['start'][0] = "Anzeigen ab";
$GLOBALS['TL_LANG']['tl_openinghours']['start'][1] = "Wenn Sie die Öffnungszeit erst ab einem bestimmten Zeitpunkt anzeigen möchten, können Sie diesen hier eingeben. Andernfalls lassen Sie das Feld leer.";
$GLOBALS['TL_LANG']['tl_openinghours']['stop'][0] = "Anzeigen bis";
$GLOBALS['TL_LANG']['tl_openinghours']['stop'][1] = "Wenn Sie die Öffnungszeit bus zu einem bestimmten Zeitpunkt anzeigen möchten, können Sie diesen hier eingeben. Andernfalls lassen Sie das Feld leer.";

