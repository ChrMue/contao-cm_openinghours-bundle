<?php

declare(strict_types=1);

/*
 * PHP version 7
 * @copyright  Christian Muenster 2022
 * @author     Christian Muenster
 * @package    CM_Openinghours
 * @license    LGPL
 * @filesource
 */

namespace ChrMue\CmOpeninghoursBundle\EventListener;

use Contao\CoreBundle\Framework\ContaoFramework;

use Contao\StringUtil;

/**
 * @internal
 */
class InsertTagsListener
{


    /**
     * @var ContaoFramework
     */
    private $framework;

    public function __construct(ContaoFramework $framework)
    {
        $this->framework = $framework;
    }

    /**
     * @return string|false
     */
    public function __invoke(string $tag, bool $useCache, $cacheValue, array $flags)
    {
        $elements = explode('::', $tag);
        $key = strtolower($elements[0]);

        switch ($name) {
            case 'cm_openingstatus': return $this->replaceOpeningStatus($arrSplit); 
                break;
            case 'cm_openinghourslist': return $this->replaceOpeningHoursList($arrSplit); 
                break;
            default: return false;
                break;
        }
        return false;
    }

    private function replaceOpeningStatus(string $parameters): string
    {
        $this->framework->initialize();
        $adapter = $this->framework->getAdapter(\ChrMue\cm_OpeningHours\Inserttags::class);

        return $adapter->replaceOpeningStatus($parameters);
    }

    private function replaceOpeningHoursList(string $parameters): string
    {
        $this->framework->initialize();
        $adapter = $this->framework->getAdapter(\ChrMue\cm_OpeningHours\Inserttags::class);

        return $adapter->replaceOpeningHoursList($parameters);
    }
}
