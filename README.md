# Öffnungszeiten

## Konfiguration der Inserttags

### Status
{{cm_openingstatus, _id_, _x_}}

_id_ ist die id des Mitglieds

_x_ dieser Parameter kann 1, 2, 3 oder 0 bzw. weggelassen werden

0 (bzw. weggelassen): nur der Status wird angezeigt
Geöffnet bzw. Geschlossen

1: wenn geöffnet ist, wird zusätzlich angezeigt, wann geschlossen wird.
Geöffnet, schließt um xx Uhr.
bei Geschlossen erfolgt keine weiteren Information

2: wenn geschlossen ist, wird zusätzlich angezeigt, wann wieder geöffnet wird.
Geschlossen, öffnet um 14:00 Uhr
ist das an einem anderen Wochentag, dann wird auch dies angezeigt.
Geschlossen, öffnet am Montag um 08:00 Uhr
bei Geöffnet erfolgt keine weiteren Information

3: 1 und 2 (die Zusatzangabe erfolgt also bei geöffnet und bei geschlossen)

### Öffnungszeiten
{{cm_openinghourslist,_id_,_w_,_t_,_b_}}

_id_ ist die id des Mitglieds

Der 2. Parameter _w_ kann 1 sein oder 0 bzw. weggelassen werden

1 bedeutet vollständige Wochentage ansonsten -> Abkürzung

Der 3. Parameter _t_ legt das Zeitformat fest, wenn nicht angegeben -> aus Einstellungen

Der 4. Parameter _b_ legt den Wochenbeginn fest. kann 1 sein oder 0 bzw. weggelassen werden

1 bedeutet dass die Liste bei Montag beginnt, ansonsten bei Sonntag.



Vielen Dank für die finanzielle Unterstützung durch die Agentur FEDERLEICHT (https://federleicht.cc/)